set -e

eval $(echo "(hello.sh
  (author Sam Sartor)
  (version v0)
  (about A fake program to test clapish arg parsing.)
  (arg INPUT (required true) (about A test positional arg.) INPUT={})
  (arg TEST (short t) (long test) (about Print INPUT.) TEST=1)
  (sub hello
    (about Say hello.)
    (arg LOUD (short l) (long loud) (about Say it loudly.) HELLO='HELLO, WORLD!')
    HELLO=\${{HELLO:-'Hello, World'}}
  )
)" | clapish "$@")

if [ -n "$TEST" ]; then
  echo $INPUT
fi

if [ -n "$HELLO" ]; then
  echo $HELLO
fi
