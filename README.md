Clapish is an experimental tool for Command Line Argument Parsing In your SHell.
It uses Rust's [clap](https://crates.io/crates/clap) library to provide all
sorts of flags, positional arguments, help info, and shell completions.

To use clapish:
1. Call with arguments.
2. Describe your CLI on stdin.
3. Get a templated script on stdout.
4. Eval it.

## Example

### `myapp_cli.txt`
```lisp
(myapp
  (version 1.0)
  (author Kevin K.)
  (about "Does (very) awesome things")
  (arg DEBUG
    (short d)
    (about Sets the level of debug information)
    flags="$flags --debug")
  (arg CONFIG
    (short c)
    (long config)
    (takes_value true)
    (about Sets a custom config file)
    load_cfg --file {})
  (arg INPUT
    (required true)
    (about Sets the input file to use)
    load_input $flags {})
  (sub test
    (about Controls testing features)
    (version 1.3)
    (author Someone E.)
    (arg VERBOSE
      (short v)
      (long verbose)
      (about Print test information verbosely)
      log_lvl=debug)
    RUST_LOG=${{log_lvl:-info}} ./output_tests))
```

### `/bin/sh`
```
$ arguments=$(clapish data.txt -c cfg.yml -d test -v < myapp_cli.txt)
$ echo $arguments
flags="$flags --debug"
load_cfg --file "cfg.yml"
load_input $flags "data.txt"
log_lvl=debug
RUST_LOG=${log_lvl:-info} ./output_tests

$ eval $arguments
```

### Help
```
$ clapish -h < myapp_cli.txt
myapp 1.0
Kevin K.
Does (very) awesome things

USAGE:
    myapp [FLAGS] [OPTIONS] <INPUT> [SUBCOMMAND]

FLAGS:
    -d               Sets the level of debug information
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --config <CONFIG>    Sets a custom config file

ARGS:
    <INPUT>    Sets the input file to use

SUBCOMMANDS:
    help    Prints this message or the help of the given subcommand(s)
    test    Controls testing features
```

## FAQ

Q: Is this only for use in my shell?

A: Nope! Clapish can be used to template any sort of text via CLI. But I mostly
use it for cli parsing in bash scripts.

Q: You said something about completions?

A: You can get clapish to write a shell completion script into `dir` with a
`CLAPISH_ZSH_COMPLETIONS_TO=dir`, `CLAPISH_FISH_COMPLETIONS_TO=dir`, etc
environment variable.

Q: Should I use this?

A: Sure! But not in production. I started this super recently and it is still
feature-incomplete and buggy
