use std::env::vars;
use std::io::{stdin, BufRead};
use std::{fs::File, io::stdout, io::Write, path::Path, process::exit};

use anyhow::{anyhow, bail, Context, Error};
use bumpalo::Bump;
use clap::{App, Arg, ArgMatches, Shell, SubCommand};

mod tokens;
use tokens::{Lexer, Token::*};

mod templ;
use templ::write_templ;

fn main() -> Result<(), Error> {
    let bump = Bump::new();
    let (mut app, list) = parse_app(&bump, &mut Lexer::new(stdin().lock()))?;
    write_completions(&mut app);

    let matches = match app.get_matches_safe() {
        Ok(m) => m,
        Err(e) => {
            // FIXME: --version still sneaks into stdout
            eprintln!("{}", e.message);
            exit(match e.use_stderr() {
                true => 1,
                false => 2,
            })
        }
    };

    let out = stdout();
    let mut out = out.lock();
    write_matches(&mut out, list, &matches)?;
    out.flush()?;
    Ok(())
}

fn write_matches(o: &mut impl Write, l: MatchList, a: &ArgMatches) -> Result<(), Error> {
    let mut line = String::new();
    for m in l {
        match m {
            Match::Arg(n, t) => {
                if write_templ(&mut line, n, t, a)? {
                    writeln!(o, "{}", line)?;
                }
            }
            Match::Sub(n, l, t) => {
                if write_templ(&mut line, n, t, a)? {
                    write_matches(o, l, a.subcommand_matches(n).unwrap())?;
                    writeln!(o, "{}", line)?;
                }
            }
        }
    }

    Ok(())
}

type MatchList<'b> = Vec<Match<'b>>;

enum Match<'b> {
    Arg(&'b str, &'b str),
    Sub(&'b str, MatchList<'b>, &'b str),
}

fn parse_arg_set<'b>(
    b: &'b Bump,
    lex: &mut Lexer<impl BufRead>,
    arg: Arg<'b, 'b>,
) -> Result<Arg<'b, 'b>, Error> {
    let arg = match lex.next_lit()? {
        "short" => arg.short(lex.bump_body(b)?),
        "long" => arg.long(lex.bump_body(b)?),
        "about" | "help" => arg.help(lex.bump_body(b)?),
        "required" => arg.required(lex.next_bool()?),
        "takes_value" => arg.takes_value(lex.next_bool()?),
        "default" => arg.default_value(lex.bump_body(b)?),
        "index" => arg.index(lex.next_ordinal()?),
        ty => bail!("unknown setting {:?}", ty),
    };
    lex.next_close()?;
    Ok(arg)
}

fn parse_app_set<'b>(
    b: &'b Bump,
    lex: &mut Lexer<impl BufRead>,
    app: App<'b, 'b>,
    list: &mut MatchList<'b>,
) -> Result<App<'b, 'b>, Error> {
    let ty = lex.next_lit()?;
    let app = match ty {
        "author" => app.author(lex.bump_body(b)?),
        "about" => app.about(lex.bump_body(b)?),
        "version" => app.version(lex.bump_body(b)?),
        "arg" => {
            let name = lex.bump_lit(b)?;
            let mut arg = Arg::with_name(name);
            loop {
                match lex.next(true, false)? {
                    Open => arg = parse_arg_set(b, lex, arg)?,
                    Close => return Ok(app.arg(arg)),
                    Lit(template) => {
                        list.push(Match::Arg(name, b.alloc_str(template)));
                        break;
                    }
                    t => bail!("expected setting or template, found {:?}", t),
                }
            }
            app.arg(arg)
        }
        "sub" => {
            let name = lex.bump_lit(b)?;
            let mut sub = SubCommand::with_name(name);
            let mut sub_list = Vec::new();
            loop {
                match lex.next(true, false)? {
                    Open => sub = parse_app_set(b, lex, sub, &mut sub_list)?,
                    Close => {
                        list.push(Match::Sub(name, sub_list, ""));
                        return Ok(app.subcommand(sub));
                    }
                    Lit(template) => {
                        list.push(Match::Sub(name, sub_list, b.alloc_str(template)));
                        break;
                    }
                    t => bail!("expected setting or template, found {:?}", t),
                }
            }
            app.subcommand(sub)
        }
        _ => bail!("unknown setting {:?}", ty),
    };
    lex.next_close()?;
    Ok(app)
}

fn parse_app<'b>(
    b: &'b Bump,
    lex: &mut Lexer<impl BufRead>,
) -> Result<(App<'b, 'b>, MatchList<'b>), Error> {
    lex.next_open()?;
    let name = lex.bump_lit(b)?;

    let mut app = App::new(name).bin_name(name);
    let mut list = Vec::new();

    loop {
        match lex.next(false, false)? {
            Open => app = parse_app_set(b, lex, app, &mut list)?,
            Close => break,
            t => bail!("expected setting, found {:?}", t),
        }
    }

    Ok((app, list))
}

fn write_completions_to_path(app: &mut App, path: &Path, shell: Shell) -> Result<(), Error> {
    if path.is_dir() {
        app.gen_completions(
            app.get_bin_name()
                .ok_or(anyhow!("unknown binary name"))?
                .to_string(),
            shell,
            path,
        );
    } else {
        app.gen_completions_to(
            path.file_stem()
                .and_then(std::ffi::OsStr::to_str)
                .ok_or(anyhow!("no file name"))?
                .trim_start_matches('_'),
            shell,
            &mut File::create(path)?,
        );
    };

    Ok(())
}

fn write_completions(app: &mut App) {
    for (name, value) in vars() {
        let mut parts = name.split("_");
        if parts.next() != Some("CLAPISH") {
            continue;
        }
        let shell: Shell = match parts.next().map(str::parse) {
            Some(Ok(s)) => s,
            _ => continue,
        };
        if parts.next() != Some("COMPLETIONS") {
            continue;
        }
        if parts.next() != Some("INTO") {
            continue;
        }
        if parts.next().is_some() {
            continue;
        }

        let path = Path::new(value.as_str());
        let res = write_completions_to_path(app, path, shell).with_context(|| {
            format!(
                "Could not write {} completions to {}",
                shell,
                path.display(),
            )
        });

        if let Err(e) = res {
            eprintln!("{}", e);
        }
    }
}
