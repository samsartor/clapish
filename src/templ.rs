use anyhow::{bail, anyhow, Error};
use clap::ArgMatches;
use std::char::from_digit;

fn escape_character(c: char, o: &mut String) {
    let unesc = match c {
        '\x07' => 'a',
        '\x08' => 'b',
        '\x0b' => 'v',
        '\x0c' => 'f',
        '\x1b' => 'e',
        '\t' => 't',
        '\r' => 'r',
        '\n' => 'n',
        '\\' | '"' | '\'' => c,
        _ => {
            let len = c.len_utf8();
            o.reserve(len * 4);

            let mut bytes = [0; 4];
            c.encode_utf8(&mut bytes);
            for byte in &bytes[..len] {
                o.push_str("\\x");
                o.push(from_digit((byte >> 4) as u32, 16).unwrap());
                o.push(from_digit((byte & 0x0f) as u32, 16).unwrap());
            }
            return;
        }
    };

    o.reserve(2);
    o.push('\\');
    o.push(unesc);
}

#[derive(Copy, Clone)]
enum EscapeMode {
    ShAnsi,
    ShSimple,
    Rust,
}

impl EscapeMode {
    pub fn from_char(c: char) -> Option<EscapeMode> {
        use EscapeMode::*;

        Some(match c {
            'a' => ShAnsi,
            's' => ShSimple,
            'r' => Rust,
            _ => return None,
        })
    }

    pub fn open(self, o: &mut String) {
        use EscapeMode::*;

        match self {
            ShAnsi => o.push_str("$'"),
            ShSimple | Rust => o.push('"'),
        }
    }

    pub fn close(self, o: &mut String) {
        use EscapeMode::*;

        match self {
            ShAnsi => o.push('\''),
            ShSimple | Rust => o.push('"'),
        }
    }

    pub fn push_char(self, c: char, o: &mut String) {
        use EscapeMode::*;

        match self {
            _ if c.is_alphanumeric() => o.push(c),
            ShAnsi if ",._- ".find(c).is_some() => o.push(c),
            ShAnsi => escape_character(c, o),
            ShSimple if "\"$`\\".find(c).is_some() => {
                o.push('\\');
                o.push(c);
            },
            ShSimple => o.push(c),
            Rust => o.extend(c.escape_default()),
        }
    }

    pub fn escape(self, s: &str, o: &mut String) {
        self.open(o);
        for c in s.chars() {
            self.push_char(c, o);
        }
        self.close(o);
    }
}

// TODO: this implementation could be a lot cleaner
pub fn write_templ(out: &mut String, n: &str, t: &str, m: &ArgMatches) -> Result<bool, Error> {
    out.clear();

    let mut cached = None;
    let mut brace = None;
    let mut mode = None;
    for c in t.chars() {
        if let Some(b) = brace {
            if b == '{' && c == '}' {
                cached = cached.or_else(|| m.value_of(n));
                if let Some(v) = cached {
                    mode.unwrap_or(EscapeMode::ShSimple).escape(v, out);
                } else {
                    return Ok(false);
                }

                mode = None;
                brace = None;
            } else if b == c {
                out.push(c);
                brace = None;
            } else if b == '{' && mode.is_none() {
                mode = Some(EscapeMode::from_char(c)
                    .ok_or(anyhow!("invalid escape mode: {}", c))?)
            } else {
                bail!("mismatched format braces");
            }
        } else if c == '{' || c == '}' {
            brace = Some(c);
        } else {
            out.push(c);
        }
    }

    Ok(cached.is_some() || m.is_present(n))
}
