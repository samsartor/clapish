use anyhow::{bail, Error};
use bumpalo::Bump;
use std::io::BufRead;
use utf8::BufReadDecoder;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Token<'s> {
    Open,
    Close,
    Lit(&'s str),
    True,
    False,
    Ordinal(u64),
    Eof,
}

pub struct Lexer<R: BufRead> {
    read: BufReadDecoder<R>,
    seg_start: usize,
    seg_end: usize,
    buf_a: String,
    buf_b: String,
    reached_eof: bool,
}

enum LexerBuf {
    A,
    B,
}

impl<R: BufRead> Lexer<R> {
    pub fn new(read: R) -> Self {
        Lexer {
            read: BufReadDecoder::new(read),
            seg_start: 0,
            seg_end: 0,
            buf_a: String::new(),
            buf_b: String::new(),
            reached_eof: false,
        }
    }

    fn refill_a(&mut self) -> Result<(), Error> {
        if !self.reached_eof {
            self.buf_a.replace_range(0..self.seg_end, "");
            self.seg_end = 0;
            self.seg_start = 0;
            match self.read.next_lossy() {
                Some(s) => self.buf_a.push_str(s?),
                None => self.reached_eof = true,
            }
        }
        Ok(())
    }

    fn next_seg(&mut self, tail: bool) -> Option<LexerBuf> {
        // consume whitespace
        if let Some(n) = self.buf_a[self.seg_end..].find(|c: char| !c.is_whitespace()) {
            self.seg_end += n;
            // starting at the end of the previous segment
            self.seg_start = self.seg_end;
        } else {
            self.seg_end = self.buf_a.len();
            self.seg_start = self.seg_end;
            return None;
        }

        // eof can act as an end paren or whitespace
        let end = match self.reached_eof {
            true => Some(self.buf_a.len() - self.seg_start),
            false => None,
        };

        let part = &self.buf_a[self.seg_start..];
        let len = match part.chars().next()? {
            '(' | ')' => 1,
            p @ '"' | p @ '\'' => {
                self.buf_b.clear();
                let mut escape = false;
                for (index, c) in part.char_indices().skip(1) {
                    if escape {
                        self.buf_b.push(match c {
                            'n' => '\n',
                            'r' => '\r',
                            't' => '\t',
                            '0' => '\0',
                            _ => c,
                        });
                        escape = false;
                    } else if c == '\\' {
                        escape = true;
                    } else if c == p {
                        // reached the end of the literal
                        self.seg_end += index + 1;
                        return Some(LexerBuf::B);
                    } else {
                        self.buf_b.push(c);
                    }
                }
                return None;
            }
            _ => {
                if tail {
                    part.find(')').or(end)?
                } else {
                    part.find(|c: char| c.is_whitespace() || c == ')').or(end)?
                }
            }
        };

        // move end of segment to the discovered length
        self.seg_end += len;
        Some(LexerBuf::A)
    }

    pub fn next(&mut self, tail: bool, eval: bool) -> Result<Token, Error> {
        use Token::*;

        let buf = loop {
            if self.reached_eof && self.seg_start == self.seg_end {
                if self.seg_end == self.buf_a.len() {
                    return Ok(Eof);
                } else {
                    bail!("unexpected eof");
                }
            }

            if let Some(buf) = self.next_seg(tail) {
                break buf;
            }
            self.refill_a()?;
        };

        let seg = match buf {
            LexerBuf::A => &self.buf_a[self.seg_start..self.seg_end],
            LexerBuf::B => self.buf_b.as_str(),
        };

        Ok(match (seg, eval) {
            ("(", _) => Open,
            (")", _) => Close,
            ("true", true) => True,
            ("false", false) => False,
            (_, true) => match seg.parse() {
                Err(_) | Ok(0) => Lit(seg),
                Ok(n) => Ordinal(n),
            },
            (_, false) => Lit(seg),
        })
    }

    pub fn next_lit(&mut self) -> Result<&str, Error> {
        match self.next(false, false)? {
            Token::Lit(text) => Ok(text),
            t => bail!("expected text, found {:?}", t),
        }
    }

    pub fn bump_lit<'b>(&mut self, bump: &'b Bump) -> Result<&'b str, Error> {
        Ok(bump.alloc_str(self.next_lit()?))
    }

    pub fn next_body(&mut self) -> Result<&str, Error> {
        match self.next(true, false)? {
            Token::Lit(text) => Ok(text),
            t => bail!("expected text, found {:?}", t),
        }
    }

    pub fn bump_body<'b>(&mut self, bump: &'b Bump) -> Result<&'b str, Error> {
        Ok(bump.alloc_str(self.next_body()?))
    }

    pub fn next_bool(&mut self) -> Result<bool, Error> {
        match self.next(false, true)? {
            Token::True => Ok(true),
            Token::False => Ok(false),
            t => bail!("expected true or false, found {:?}", t),
        }
    }

    pub fn next_ordinal(&mut self) -> Result<u64, Error> {
        match self.next(false, true)? {
            Token::Ordinal(n) => Ok(n),
            t => bail!("expected true or false, found {:?}", t),
        }
    }

    pub fn next_open(&mut self) -> Result<(), Error> {
        match self.next(true, false)? {
            Token::Open => Ok(()),
            t => bail!("expected start paren, found {:?}", t),
        }
    }

    pub fn next_close(&mut self) -> Result<(), Error> {
        match self.next(true, false)? {
            Token::Close => Ok(()),
            t => bail!("expected end paren, found {:?}", t),
        }
    }
}
